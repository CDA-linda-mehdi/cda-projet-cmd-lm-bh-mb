package projet.commandes;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class CD {
	private String nom;


	//Lister dossiers d'un repertoire
	private static void listeElementDansUnDossier3(String dossier){

		ArrayList<String> dossiers=new ArrayList<>();


		File monDossier = new File(dossier);



		// objet monDossier de type File et represente un chemin vers le param�tre dossier
		File[] liste = monDossier.listFiles();// liste represente la liste des fichiers contenus dans monDossier
		for(File elementListe : liste) // je parcours la liste de fichiers contenus dans la liste et les mets dans la variable elementListe 
		{
			if(elementListe.isDirectory() && elementListe.getName().equals(dossier) ) // si elementListe est un dossier, je l'ajoute � mon ArrayList dossiers
			{
				System.out.println("<DIR> "+ elementListe);
				dossiers.add(elementListe.getName());

			}
		}
	}

	public static void afficheDossier3() {
		listeElementDansUnDossier3(CmdPwd.getDossierEnCours());


	}   

	
	
	// commande cd
	public static void modifDossierEnCours(String nomDossier) { // modifie DossierEncours sans l'afficher car void

		File monDossier = new File(CmdPwd.getDossierEnCours());
		File[] liste = monDossier.listFiles();
		boolean b =true;

		for(File elementListe : liste) {

			if(elementListe.isDirectory() && elementListe.getName().equals(nomDossier)) {

				CmdPwd.setDossierEnCours(CmdPwd.getDossierEnCours() + nomDossier +"\\" );
				b =true; // b sert uniquemnt lorsqu'il trouve que mondossierencours = nomDossier
				break;
			}else {
				
				b = false; // 
			}


		}

		if(!b) { // cette condition permet de n'imprimer qu'une le message
			
			System.out.println("Le chemin d�acc�s sp�cifi� est introuvable. ");
		}

	}


	public static void cdRetour() { // modifie DossierEncours sans l'afficher car void

		StringBuilder a = new StringBuilder(); // string builder pour manipuler pllus facilement un string
		a.append(CmdPwd.getDossierEnCours()); // je passe le dossier en cours dans le string builder
		int compte = CmdPwd.getDossierEnCours().length();
		for (int i = compte-1; i > 0; i--) {
			if (i == compte-1 || a.charAt(i) != '\\') { // si i egale taille du string en cours et si le charachter en cours est different de '/'
				a.deleteCharAt(i);
			}else if(a.charAt(i) == '\\') { // sinon je break quand je rencontre le '/'
				break;
			}

		}

		CmdPwd.setDossierEnCours(a.toString()); // modifie le dossier en cours
	}



	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}	

}
