package projet.commandes;

public class ExceptionVide extends Exception {

	private String message;
	
	public ExceptionVide(String message) {
		super(message);
		
	}
}
