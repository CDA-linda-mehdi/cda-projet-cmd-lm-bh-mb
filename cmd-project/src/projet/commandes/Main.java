package projet.commandes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Main {

	public static void main(String[] args) throws IOException {
		
		if(System.getProperties().get("cdi.default.folder") != null) {
	           if(!(System.getProperties().get("cdi.default.folder").equals(""))) {
	               String str = "" +System.getProperties().get("cdi.default.folder");
	               File file = new File(str);
	               if(file.isDirectory()) {
	                   CmdPwd.setDossierEnCours(str);;
	               }
	           }
	       }
		

		boolean exe = true;
		String r1 = "";
		String r2 = "";
		Scanner sc = new Scanner(System.in);
		while (exe) {

			// focus pour le terminal
			String entrer = "> ";
			System.out.print(entrer);
			entrer = sc.nextLine();

			Date a = new Date();

			// set
			// historique--------------------------------------------------------------------

			CmdHistory.remplirHistorique(a, entrer);

			// -------------------------DEBUT TRAITEMENT----------------------------------------------

			// recupere le nbre dans le string pour le parametre

			if (entrer.contains("isprime")) {
				r1 = "";
				r2 = "";
				for (int i = 0; i < entrer.length(); i++) {
					if (Character.isDigit(entrer.charAt(i))) {
						r1 += entrer.charAt(i);
					}
				}
				entrer = "isprime";

			} else if (entrer.contains("river")) {
				boolean vrai = true;
				r1 = "";
				r2 = "";
				for (int i = 0; i < entrer.length(); i++) {
					if (Character.isDigit(entrer.charAt(i)) && vrai) {
						r1 += entrer.charAt(i);

					} else if (Character.isDigit(entrer.charAt(i)) && !vrai) {
						r2 += entrer.charAt(i);
					}

					if (entrer.charAt(i) == ' ' && !r1.isEmpty()) {
						vrai = false;
					}
				}
				entrer = "river";

			} else if (entrer.contains("cd ")) {
				r1 = "";
				for (int i = 3; i < entrer.length(); i++) {

					r1 += entrer.charAt(i);
				}

				entrer = "cd";

			} else if (entrer.contains("find -ends")) {
				r1 = "";
				for (int i = 10; i < entrer.length(); i++) {

					r1 += entrer.charAt(i);
				}

				entrer = "find -ends";
			} else if (entrer.contains("find -starts")) {
				r1 = "";
				for (int i = 12; i < entrer.length(); i++) {

					r1 += entrer.charAt(i);
				}

				entrer = "find -starts";
			}else if (entrer.contains("find")) {
				r1 = "";
				for (int i = 5; i < entrer.length(); i++) {

					r1 += entrer.charAt(i);
				}

				entrer = "find";
			} else if (entrer.contains("copy")) {
				r1 = "";
				r2 = "";
				boolean vrai = true;
				for (int i = 5; i < entrer.length(); i++) {
					if (vrai && entrer.charAt(i) != ' ') {
						r1 += entrer.charAt(i);
					} else if (!vrai && entrer.charAt(i) != ' ') {
						r2 += entrer.charAt(i);
					} else if (entrer.charAt(i) == ' ') {
						vrai = false;
					}
				}

				entrer = "copy";

			}else if (entrer.contains("read")) {
				r1 = "";
				for (int i = 5; i < entrer.length(); i++) {

					r1 += entrer.charAt(i);
				}

				entrer = "read";

			}else if (entrer.contains("crf")) {
				r1 = "";
				for (int i = 4; i < entrer.length(); i++) {

					r1 += entrer.charAt(i);
				}

				entrer = "crf";
			}


			else if (entrer.contains("crd")) {
				r1 = "";
				for (int i = 4; i < entrer.length(); i++) {

					r1 += entrer.charAt(i);
				}

				entrer = "crd";
			}			


			// -------------------------------FIN TRAITEMENT-----------------------------------------------



			// ---------------------------------DEBUT SWITCH-----------------------------------------


			switch (entrer) {
			case "help":

				CmdHelp.help();

				break;

			case "isprime":
				try {
					CmdIsprime.isprime(Integer.parseInt(r1));
				} catch (NumberFormatException e) {
					System.out.println("vous n'avez pas mis les parametres");
				}
				break;

			case "dir":

				CmdDir.afficheDossier();

				break;

			case "river":
				try {
					CmdRiver.river(Integer.parseInt(r1), Integer.parseInt(r2));
				} catch (NumberFormatException e) {
					System.out.println("vous n'avez pas mis les parametres");
				}

				break;

			case "history":
				CmdHistory.history();

				break;

			case "pwd":
				System.out.println(CmdPwd.getDossierEnCours());

				break;

			case "quit":

				exe = false;
				System.out.println("terminal ferm�");

				break;

			case "histclear":

				CmdHistory.histclear();

				break;

			case "dirng":

				CmdDirng.afficheDossier2();

				break;

			case "cd":

				if (r1 != "") {
					CD.modifDossierEnCours(r1);
				} else {
					System.out.println("entrer un nom de dossier");
				}

				break;

			case "cd..":

				CD.cdRetour();

				break;

			case "find -ends":
				try {

					if (r1.isEmpty()) {
						ExceptionVide erreur = new ExceptionVide("Le param�tre entr� n'est pas correct.");
						throw erreur;

					}
					Find.findEnd(CmdPwd.getDossierEnCours(), r1);
				} catch (ExceptionVide e) {
					System.out.println(e.getMessage());
				}

				break;

			case "find -starts":
				try {

					if (r1.isEmpty()) {
						ExceptionVide erreur = new ExceptionVide("Le param�tre entr� n'est pas correct.");
						throw erreur;

					}
					Find.findStart(CmdPwd.getDossierEnCours(), r1);
				} catch (ExceptionVide e) {
					System.out.println(e.getMessage());
				}


				break;

			case "find":

				Find.find(CmdPwd.getDossierEnCours(), r1);

				break;

			case "read":

				CmdCat.lireFichier(r1);

				break;

			case "copy":
				if (r1.equals(r2)) {
					System.out.println("Vous ne pouvez pas �craser votre fichier source.");
				} else {
					try {
						if (r1.isEmpty() || r2.isEmpty()) {
							ExceptionVide erreur = new ExceptionVide("Vous n'avez pas correctement entr� le fichier source ou le fichier de destination.");
							throw erreur;
						}

						CmdCopy.copy(r1, r2);
					} catch (ExceptionVide e) {
						System.out.println(e.getMessage());
					}
				}

				break;

			case "crf":

				CmdCopy.crf(r1);

				break;

			case "crd":

				CmdCopy.crd(r1);

				break;	

			case "getvars -env":
				CmdVarEnv.getvarsEnv();
				break;	
				
			case "getvars -prop":
				CmdVarEnv.getvarsProp();
				break;
				
			case "getvars":
				System.out.println("************** variables d'environnement*****************");
				System.out.println("*********************************************************");
				CmdVarEnv.getvarsEnv();
				
				System.out.println("************** proprietes du Systeme*****************");
				System.out.println("*********************************************************");
				CmdVarEnv.getvarsProp();				
				break;
				
			case "getvars -env -prop":
								
				System.out.println("************** variables d'environnement*****************");
				System.out.println("*********************************************************");
				CmdVarEnv.getvarsEnv();
				
				System.out.println("************** proprietes du Systeme*****************");
				System.out.println("*********************************************************");
				CmdVarEnv.getvarsProp();
				break;
			
			case "getvars -prop -env":
				System.out.println("************** proprietes du Systeme*****************");
				System.out.println("*********************************************************");
				CmdVarEnv.getvarsProp();
				
				System.out.println("************** variables d'environnement*****************");
				System.out.println("*********************************************************");
				CmdVarEnv.getvarsEnv();
				
				break;
				

			default:
				System.out.println("vous n avez pas entr� la bonne commande");
				break;
			}
		}
		sc.close();
	}

}
