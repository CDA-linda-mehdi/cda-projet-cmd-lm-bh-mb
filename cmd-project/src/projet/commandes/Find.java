package projet.commandes;

import java.io.File;

public class Find {


	public static void findEnd(String path, String nom_fichier) {
		IntegerCda compte = new IntegerCda(0);		
		//recursive(path, nom_dossier, compte);

		String commande = "end";
		
		//compte += recursive(path, nom_fichier, compte);
		recursive(path, nom_fichier, compte,commande);
		
		//affichage du compte final trouv� avec la recursive
		System.out.println(compte);
	}


	public static IntegerCda recursive(String path, String nom_fichier, IntegerCda compte,String commande) { // parametres  : path (chemin en cours = au dep) et String (dossier que l'on cherche)

		File monDossier = new File(path); // je recupere tous les fichiers point� par le chemin en cours et je le mets dans monDossier
		File[] liste = monDossier.listFiles(); // 
		for(File a : liste) {
			if (a.isFile()) {  // si ce n'est pas un repertoire
				if(commande.equals("end")) {

					if (a.getName().endsWith(nom_fichier)) { // je recupere tout ce qui commence par Nou
						//System.out.println(""+a.getName());
						compte.val++; // des que j'ai trouv�, j'incremente le compteur de 1
						//System.out.println(compte+ " iiiiiiiiiiiii");
						System.out.println(a.getAbsolutePath());
					}

				} else if(commande.equals("start")) {

					if (a.getName().startsWith(nom_fichier)) { // je recupere tout ce qui commence par Nou
						//System.out.println(""+a.getName());
						compte.val++; // des que j'ai trouv�, j'incremente le compteur de 1
						//System.out.println(compte+ " iiiiiiiiiiiii");
						System.out.println(a.getAbsolutePath());
					}
				} else if(commande.equals("find")) {

					if (a.getName().equals(nom_fichier)) { // je recupere tout ce qui commence par Nou
						//System.out.println(""+a.getName());
						compte.val++; // des que j'ai trouv�, j'incremente le compteur de 1
						//System.out.println(compte+ " iiiiiiiiiiiii");
						System.out.println(a.getAbsolutePath());
					}
				}


			}else { // si c'est un repertoire
				recursive(path+a.getName()+"\\",nom_fichier, compte,commande);
			}
		}
		//System.out.println(res );
		//System.out.println(compte);

		return compte; // compteur final
	}

	public static void findStart(String path, String nom_fichier) {
		
		IntegerCda compte = new IntegerCda(0);
		
		String commande = "start";
		
		recursive(path, nom_fichier, compte, commande);
		
		System.out.println(compte);

	}
	

	public static void find(String path, String nom_fichier) {

		IntegerCda compte = new IntegerCda(0);
		
		String commande = "find";
		
		recursive(path, nom_fichier, compte, commande);
		
		System.out.println(compte);
	}

}
