package projet.commandes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;

public class CmdCopy {

	public static void copy(String nomSrc, String nomDest) throws IOException {

		// copy avec copy de Files avec gestion de l'exception si fichier n'existe pas
		try {
			Files.copy(Paths.get(CmdPwd.getDossierEnCours() + "\\"+ nomSrc),Paths.get(CmdPwd.getDossierEnCours() + "\\"+ nomDest));
			System.out.println("Copie effectu�e !");
		} catch(NoSuchFileException e) {
			System.out.println("fichier inexistant!");

		}
	}



	// copy avec methode stream
	//		
	//		File monDossier = new File(CmdPwd.getDossierEnCours()); // je recupere tous les fichiers point� par le chemin en cours et je le mets dans monDossier
	//		boolean trouve = false;
	//		File[] liste = monDossier.listFiles(); //
	//		for (File file : liste) {
	//			if (file.getName().equalsIgnoreCase(nomSrc)) {
	//				
	//				copyAux(file, new File(CmdPwd.getDossierEnCours()+"/"+nomDest+"/"));
	//				trouve = true;
	//				break;
	//			}
	//		}
	//		if(trouve) {
	//			System.out.println("ok");
	//		} else {
	//			System.err.println("fichier introuvable");
	//		}


	private static boolean copyAux(File source, File dest) throws IOException {
		try {
			FileChannel in = new FileInputStream(source).getChannel();//on instancie l'objet "in" de type FileInputStream qui h�rite de la classe FileChannel
			FileChannel out = new FileOutputStream(dest).getChannel();//la m�thode getChannel r�cup�re toutes les donn�es en byte (afin de les manipuler)

			in.transferTo(0, in.size(), out);//on copie le fichier "in" de son d�but (0) � sa fin (size) vers le fichier de destination (out)
		} catch (IOException erreur){
			System.out.println("probl�me d'entr�e");
		}


		return true;
	}

	// cr�er un fichier dans le repertoire en cours

	public static void crf(String fichier) throws IOException {
		File monDossier = new File(CmdPwd.getDossierEnCours());
		File[] liste = monDossier.listFiles();

		try {
			File file = new File(CmdPwd.getDossierEnCours()+"\\"+fichier); // je cree un objet file depuis mon dossier en cours
			file.createNewFile();//methode qui genere une erreur, il faut la gerer avec try et catch

		}catch (IOException erreur){
			System.out.println("fichier existe deja");
		}
	}

	// cr�er un repertoire dans le repertoire en cours

	public static void crd(String dossier) {

		File monDossier = new File(CmdPwd.getDossierEnCours()); // objet monDossier de type File et represente un chemin vers le param�tre dossier
		File[] liste = monDossier.listFiles();// liste represente la liste des fichiers contenus dans monDossier

		for(File elementListe : liste) {
			if(elementListe.getName().equals(dossier)) {
				System.out.println("le repertoire existe deja");
			}else {
				File repertoire = new File(CmdPwd.getDossierEnCours()+"\\"+dossier);
				repertoire.mkdir(); // la methode ne genere pas d'erreur, il faut mettre une condition
				System.out.println("repertoire ajout�");
			}        	
		}		
	}
}

