package projet.commandes;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TreeMap;

public class CmdHistory {

	private String nom;//on initialise un attribut private de type String nomm�e "nom"
	public static TreeMap<Date, String> historique = new TreeMap<>();//on initialise un attribut de classe public de type TreeMap nomm�e "historique" qui a pour ses key le type Date et pour ses value le type String. Elle contiedra l'historique de la CMD.

	public String getNom() {//on g�n�re une m�thode "get" qui renvoie la valeur de l'attribut "nom"
		return nom;
	}

	// M�thode qui compl�te la TreeMap "historique"
	public static void remplirHistorique(Date a, String entrer) {
		Date echange = a;//variable "a" qui va retenir une date
		if ((CmdHistory.historique.size() < 10 && entrer.equals("isprime")) 
				&& (CmdHistory.historique.size() < 10 && entrer.equals("river"))
				 && (CmdHistory.historique.size() < 10 && entrer.equals("find"))
				 && (CmdHistory.historique.size() < 10 && entrer.equals("find -ends"))
				 && (CmdHistory.historique.size() < 10 && entrer.equals("find -starts"))
				 && (CmdHistory.historique.size() < 10 && entrer.equals("cd"))) {//conditions qui emp�che que certaines commandes n'entrent dans la TreeMap "historique"
			CmdHistory.historique.put(a, entrer);//la date (key) et la commande (value) sont ajout�es � la TreeMap "historique"

		}else if((CmdHistory.historique.size() > 9 && entrer.equals("isprime")) 
			&& (CmdHistory.historique.size() > 9 && entrer.equals("river"))
			 && (CmdHistory.historique.size() > 9 && entrer.equals("find"))
			 && (CmdHistory.historique.size() > 9 && entrer.equals("find -ends"))
			 && (CmdHistory.historique.size() > 9 && entrer.equals("find -starts"))
			 && (CmdHistory.historique.size() > 9 && entrer.equals("cd"))){//condition qui bloque la TreeMap � un maximum de 10 entr�es et � certaines commandes
			Set <Date> key = CmdHistory.historique.keySet();//on met les dates contenues dans la TreeMap "historique" dans une nouvelle liste Set nomm�e "key"
			for (Date b : key) {//la variable "b" retient chaque date contenue dans la liste "key" parcourue
				for (Date c : key) {//la variable "c" retient aussi chaque date contenue dans la liste "key" parcourue
					if(b.before(c)) {//on compare les diff�rences de valeurs qu'il y a entre les deux dates
						echange = b;//si "b" est plus ancienne que "c", la date est retenue dans la variable "echange"
						
					}else {
						echange = c;//si "c" est plus ancienne que "b", la date est retenue dans la variable "echange"
					}
				}
			}
			CmdHistory.historique.remove(echange);//on supprime de la TreeMap "historique" l'entr�e key/value correspondant � la date (key) retenue dans "echange"
			CmdHistory.historique.put(a, entrer);//et on ajoute la date (key) et la commande (value) dans la TreeMap "historique"
		}
	}


	//M�thode qui affiche la liste de date format�e et le nom de chaque commande de la TreeMap "Historique"
	public static void history() {
		Set <Date> key = CmdHistory.historique.keySet();//on initialise une liste Set nomm�e "key" de type date qui reprend les dates contenues dans la TreeMap "historique"
		SimpleDateFormat a = new SimpleDateFormat("HH:mm:ss dd/MMMMM/yyyy");//on initialise le format de la date dans une variable "a"

		for (Date b : key) {//on parcours la liste "key" pour mettre chaque date dans la variable "b"
			String date = a.format(b);//on format la date contenu dans "b" avec le m�me format contenu dans "a"
			System.out.println("la date : [ " + date + " ] la commande : [ "+ historique.get(b)+" ]");//on affiche la date (key) et la commande (value) contenues dans la TreeMap "historique" 

		}
	}
	
	//M�thode qui vide la TreeMap "Historique"
	public static void histclear() {
		CmdHistory.historique.clear();//on vide la TreeMap "historique"
		
	}	
}
