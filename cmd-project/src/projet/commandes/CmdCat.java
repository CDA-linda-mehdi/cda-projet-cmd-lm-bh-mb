package projet.commandes;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CmdCat {


	public static void lireFichier(String nom_fichier) throws IOException {



		File monDossier = new File(CmdPwd.getDossierEnCours()); // je cr�e un path DossierEncCours
		File[] liste = monDossier.listFiles();


		for(File a : liste) {
			if (a.isFile()) { 
				if (a.getName().equals(nom_fichier)) {

					BufferedReader lecteur = new BufferedReader(new FileReader(a.getAbsolutePath()));

					String ligne;

					while((ligne = lecteur.readLine()) !=null ) {
						System.out.println(ligne);

					}
					lecteur.close();
				}
			}
		}		
	}
}
