package projet.commandes;

import java.util.TreeMap;

public class CmdHelp {

	private String nom;
	private static TreeMap<String, String> list = new TreeMap<>();

	public String getNom() {
		return nom;
	}
	
	public static void help() {
		list.put("DIR", "        - Affiche la liste des dossiers et fichiers du r�pertoires C:");
		list.put("HELP", "       - Affiche des informations sur les commandes");
		list.put("HISTCLEAR", "  - Efface l'�cran");
		list.put("HISTORY", "    - Affiche les dix derni�res commandes");
		list.put("ISPRIME", "    - Indique si un nombre est un nombre premier");
		list.put("QUIT", "       - Sortie du programme");
		list.put("RIVER", "      - Affiche le point d'intersection de deux rivi�res");
		list.put("PWD", "        - Commande pas encore impl�ment�es");
		
		for (String str : list.keySet()) {
			System.out.println(str+ " " +list.get(str));
		}
	}
}