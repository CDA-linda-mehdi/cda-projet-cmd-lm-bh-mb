package afpa.console.command.criteres;

import afpa.console.exception.CommandException;
import afpa.console.exception.ParametresIncorrectesException;

public class CritereNow {

	public static int create(String params) throws CommandException {
		final String[] options = params.split(" ");
		String res = "";
		if (options[0].isEmpty()) {
			res = "td";
		} else if(options.length == 1) {
			if(options[0].equals("-t")) {
				res ="t";
			}else if(options[0].equals("-d")) {
				res = "d";
			}else if(options[0].equals("-td")||options[0].equals("-dt")) {
				res ="td";
			}else {
				options[0] = options[0].replace("t", "");
				options[0] = options[0].replace("d", "");
				throw new ParametresIncorrectesException("unknown option " + options[0]);
			}
		}else if(options.length == 2) {

			if(options[0].equals("-t") && options[1].equals("-d")) {
				res ="td";
			}else if(options[0].equals("-d") && options[1].equals("-t")) {
				res = "td";
			}else {
				options[0] = options[0].replace("t", "");
				options[0] = options[0].replace("d", "");
				options[1] = options[1].replace("t", "");
				options[1] = options[1].replace("d", "");
				if(options[0].equals("-")) {
					options[0] = "";
				}else {
					res = options[0] + " ";
				}

				if(options[1].equals("-")) {
					options[1] = "";
				}else {
					res = res + options[1];
				}
				throw new ParametresIncorrectesException("unknown option " + res);
			}
		}else {
			throw new ParametresIncorrectesException("unknown option " + options[0]);
		}
		
		if(res.equals("t")) {
			return 0;
		}else if(res.equals("d")) {
			return 1;
		}else if(res.equals("td")) {
			return 2;
		}
		
		return -1;

	}

}



