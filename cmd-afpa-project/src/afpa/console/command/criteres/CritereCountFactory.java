package afpa.console.command.criteres;

import java.util.ArrayList;
import java.util.List;

import afpa.console.exception.CommandException;
import afpa.console.exception.OptionsIncorrectesException;
import afpa.console.exception.ParametresIncorrectesException;

public class CritereCountFactory {
	
	public static List<Critere> create(String params) throws CommandException {
		
		final String[] options = params.split(" ");
		final List<Critere> res = new ArrayList<>();
		if (options[0].isEmpty()) {
			
			res.add(new CritereFichierDossier(""));

		} else {
			Critere c = null;
			for (int i = 0; i < options.length; i++) {
				if (options[i].startsWith("-")) {
					 if ("-f".equals(options[i])) {
						c = new CritereFichier(options[i]);
						

					} else if ("-d".equals(options[i])) {
						c = new CritereDossier(options[i]);
						
					} else if (options.length == 0 || "-df".equals(options[i]) || "-fd".equals(options[i])) {
						c = new CritereFichierDossier(options[i]);
						
					}else {
						throw new OptionsIncorrectesException("unknown option " + options[i]);
					}
				} else {
					throw new OptionsIncorrectesException("unknown option " + options[i]);
					}
				res.add(c);
			}
			
		}
		return res;
	}

	
	
	
}
