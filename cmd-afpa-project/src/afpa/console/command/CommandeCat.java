package afpa.console.command;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.MalformedInputException;
import java.nio.charset.StandardCharsets;
import java.nio.file.AccessDeniedException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;


public class CommandeCat extends AbstractCommandeAvecParam implements IHistoriqueCommand  {

	public static final String CMD = "cat";
	private static final String DESC = "Affiche le contenue d'un fichier";
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}
	
	public CommandeCat(String c) {
		super(c,CMD);
	}

	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		
		Path chemin = CommandeFactory.CURRENT_FILE.toPath();
		DirectoryStream<Path> stream;
		
		try {
			stream = Files.newDirectoryStream(chemin);
			for (Path path : stream) {
				if(path.getFileName().toString().toLowerCase().equals(this.parameter) ) {
					InputStream in = Files.newInputStream(path);
				    CharsetDecoder decoder = StandardCharsets.UTF_8.newDecoder();
				    decoder.onMalformedInput(CodingErrorAction.REPLACE);
				    Reader isReader = new InputStreamReader(in, decoder);
				    BufferedReader reader = new BufferedReader(isReader);
				    String line;
					System.out.println("***************** "+path.getFileName()+" ***************** ");
					while((line = reader.readLine()) != null) {
						System.out.println(line);
					}
					System.out.println("***************** fin ***************** ");
					break;
				}
			}
		
		}catch(AccessDeniedException e) {
			System.out.println("cat : "  + " est un repertoire");
			
		}catch(MalformedInputException e) {
			System.out.println("cat : erreur format du fichier inconnu");
			
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("cat : IOException");
		}
		
		
	}
	
}
