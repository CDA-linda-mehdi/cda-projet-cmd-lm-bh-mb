package afpa.console.command;



import java.io.File;
import java.util.List;

import afpa.console.command.criteres.Critere;
import afpa.console.command.criteres.CritereCountFactory;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;
import afpa.console.exception.ParametresIncorrectesException;

class CommandeCount extends AbstractCommandeAvecParam implements IHistoriqueCommand {

	public static final String CMD = "count";
	private static final String DESC = "Affiche le nombre de fichier et dossier du repertoire en cours.";


	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeCount(String c) {
		super(c, CMD);
	}



	private void count(File file, List<Critere> criteres) {
		boolean res = false;
		boolean res2 = false;
		int nbr =0;
		int nbr2 = 0;
		Critere c;

		for (Critere critere : criteres) {

			for (final File fIn : file.listFiles()) {

				if (critere.test("-f")) {
					if (fIn.isFile()) {
						nbr ++;
						res = true;
					}
				}else if (critere.test("-d")){
					if (!fIn.isFile()) {
						nbr2 ++;
						res2 = true; 
					}
				}else if (critere.test("-df") || critere.test("-fd") || critere.test("")){
					if (!fIn.isFile()) {
						nbr2 ++;
						res2 = true; 
					}else {
						nbr ++;
						res = true;
					}
				}
			}

		}
		
		if(res) {
			System.out.println(nbr+" fichiers");
		}
		if(res2) {
			System.out.println(nbr2+" dossiers");
		}
	}




	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		
		List<Critere> criteres = CritereCountFactory.create(this.parameter);

		this.count(CommandeFactory.CURRENT_FILE, criteres);



	}
}
