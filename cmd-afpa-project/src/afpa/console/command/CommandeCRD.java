package afpa.console.command;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import afpa.console.exception.CommandException;


public class CommandeCRD extends AbstractCommandeAvecParam {
	
	public final static String CMD = "crd" ;
	private static String DESC = "creer un repertoire dans le repertoire en cours" ;

		
	 // ajout class Crd dans help
	
		public CommandeCRD(String c) {
			super(c, CMD);
		// TODO Auto-generated constructor stub
	}

		static {
			CommandeFactory.addCommandeDesc(CMD, DESC);
		}
		

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void run() throws CommandException {		
		
			File monDossier = new File(CommandeFactory.CURRENT_FILE.getName()+"\\"+this.parameter); // objet monDossier de type File et represente un chemin vers le param�tre dossier
			File[] liste = CommandeFactory.CURRENT_FILE.listFiles();// liste represente la liste des fichiers contenus dans monDossier
			boolean vrai = false;
			for(File elementListe : liste) {
				
				// je recupere le 1ere parametre qui ecrit au clavier apres crd et avant le 2�me parametre
				if(elementListe.getName().equals(this.parameter)) {
					vrai = false;
					break;
				}else {
					vrai = true;
				}       	
			}
			if(vrai) {
				monDossier.mkdir();
				System.out.println("repertoire ajout�");
			}else {
				System.out.println("le repertoire existe deja");
			}
			
		}
	
	 // reconnait la commande crd
	
		public static void chargerStaticPortion() {
			// TODO Auto-generated method stub
			
		}
		
		// TODO Auto-generated method stub
		

		public static void setDESC(String dESC) {
			DESC = dESC;
		}

	}


