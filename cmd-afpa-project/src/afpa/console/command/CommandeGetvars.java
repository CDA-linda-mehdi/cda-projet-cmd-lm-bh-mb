
package afpa.console.command;

import java.util.Map;
import java.util.Properties;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeGetvars extends AbstractCommandeAvecParam implements IHistoriqueCommand {
	
	public static final String CMD = "getvars";
	private static final String DESC = "Affiche les variable d'environnement et de la VM.";
	public static Properties liste = System.getProperties();
	
	public static void chargerStaticPortion() {
	}
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}
	
	public CommandeGetvars(String c) {
		super(c, CMD);
	}
	
	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		
		if(this.parameter.equals("-env")) {
			Map<String, String> env = System.getenv();
			System.out.println("\n-- listing env --");
			for (String envName : env.keySet()) {
				System.out.println(String.format("%-60s",envName)+String.format("%10s",env.get(envName)));
			}
			
		}else if(this.parameter.equals("-prop")) {
			liste.list(System.out);
		}else {
			System.out.println("\n-- listing env --");
			Map<String, String> env = System.getenv();
			for (String envName : env.keySet()) {
				System.out.println(String.format("%-60s",envName)+String.format("%10s",env.get(envName)));
			}
			System.out.println("\n");
			Properties liste= System.getProperties();
			liste.list(System.out);
		}

	}
	

}
