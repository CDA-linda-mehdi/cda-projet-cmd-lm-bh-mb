package afpa.console.command;

import java.text.SimpleDateFormat;
import java.util.Date;

import afpa.console.command.criteres.CritereNow;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

class CommandeNow extends AbstractCommandeAvecParam implements IHistoriqueCommand {

	public static final String CMD = "now";
	private static final String DESC = "Affiche la date et l'heure en cours";

	public CommandeNow(String c) {
		super(c, CMD);
	}

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		int flag =  CritereNow.create(this.parameter);
		Date date = new Date();

		if(flag == 0) {
			SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss");
			System.out.println(SDF.format(date));
		}else if(flag == 1) {
			SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yy");
			System.out.println(SDF.format(date));
		}else if (flag == 2) {
			SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
			System.out.println(SDF.format(date));
		}
	}
}
