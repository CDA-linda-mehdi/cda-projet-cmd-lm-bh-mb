package afpa.console.command;

import afpa.console.exception.CommandException;

public class CommandeRiver extends AbstractCommandeAvecParam{
	
	
	
	public static final String CMD = "river";
	private static final String DESC = "Une rivi�re num�rique est une s�quence de nombres o� chaque num�ro est suivi du m�me nombre plus la somme de ses chiffres.";
	
	

	public CommandeRiver(String c) {
		super(c, CMD);
		
	}
	
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void run() throws CommandException {
	
		
		
		long un = Integer.parseInt(this.parameter.substring(0, this.parameter.indexOf(' ')));
		long deux = Integer.parseInt(this.parameter.substring(this.parameter.indexOf(' ')+1,this.parameter.length()));
	
			long res = 0;
			long r1 = un;
			long r2 = deux;
			long aide = 0;
			String rr = "";
			if(r1 < r2) {
				rr = "" + r1;
				res=r1;
				aide=r2;
			}else {
				rr = "" + r2;
				res=r2;
				aide = r1;
			}

			do {

				long test = 0;
				for (int i = 0; i < rr.length(); i++) {

					test += Character.getNumericValue(rr.charAt(i));

				}

				res = res+test;
				rr = ""+ res;

				if (res > aide) {

					long transfere =res;
					res = aide;
					aide = transfere;
					rr = "" +res;

				}
			}while(res != aide);

			System.out.println(res);
		}

	
}
