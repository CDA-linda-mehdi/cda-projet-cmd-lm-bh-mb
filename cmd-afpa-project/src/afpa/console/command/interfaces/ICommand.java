package afpa.console.command.interfaces;

import java.io.IOException;

import afpa.console.exception.CommandException;

public interface ICommand {
	String getName();

	void run() throws CommandException;
}
