package afpa.console.command;

import java.io.File;
import java.io.IOException;

import afpa.console.exception.CommandException;

public class CommandeCrf extends AbstractCommandeAvecParam{
	
	
	public static final String CMD = "crf";
	private static final String DESC = "Cr�e un neauveau fichier";
	
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}
	

	public CommandeCrf(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		try {
		File nouveauFichier = new File (CommandeFactory.CURRENT_FILE +"\\"+this.parameter);
		nouveauFichier.createNewFile();
		System.out.println("ajouter !!");
		} catch(IOException e) {
			e.getMessage();
		}
		
	}

}
