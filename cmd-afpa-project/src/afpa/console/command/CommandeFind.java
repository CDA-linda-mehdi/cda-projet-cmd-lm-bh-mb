package afpa.console.command;

import java.io.File;
import java.util.List;

import afpa.console.command.criteres.Critere;
import afpa.console.command.criteres.CritereFactory;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

class CommandeFind extends AbstractCommandeAvecParam implements IHistoriqueCommand {

	public static final String CMD = "find";
	private static final String DESC = "Quitte l'interpréteur de commandes.";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeFind(String c) {
		super(c, CMD);
	}

	private int find(File file, List<Critere> criteres) {
		boolean res;
		int nbr =0;
		
		for (final File fIn : file.listFiles()) {
			res = true;
			if (fIn.isFile()) {
				for (final Critere c : criteres) {
					res &= c.test(fIn.getName());
					if(!res) {break;}
				}
				if(res) {
                    System.out.println(fIn.getAbsolutePath());
                    nbr++;
                }
			}else {
				 nbr = nbr + find(fIn,criteres);
			}
		}
		return nbr;
	}

	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		int nbr=0;
		final List<Critere> criteres = CritereFactory.create(this.parameter);
		nbr = this.find(CommandeFactory.CURRENT_FILE, criteres);
		System.out.println(nbr + " fichiers trouv�s");
	}

}
