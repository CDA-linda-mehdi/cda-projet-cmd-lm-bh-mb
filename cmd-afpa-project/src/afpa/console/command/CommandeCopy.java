package afpa.console.command;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import afpa.console.exception.CommandException;


public class CommandeCopy extends AbstractCommandeAvecParam {
	
	public final static String CMD = "copy" ;
	private static String DESC = "copie un fichier dans un autre" ;
	
	// c correspond � ce que je tape sur le clavier ==> copy file1 file2
	// CM correspond � "copy"
	
	public CommandeCopy(String c) {	
		
		super(c, CMD);
		
		
		// TODO Auto-generated constructor stub
	}
	
	 // ajout class Copy dans help
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}
	
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	
	// methode qui copie un fichier dans un autre
	public void run() throws CommandException {			
		
		try {
			Files.copy(Paths.get(CommandeFactory.CURRENT_FILE + "\\"+ this.parameter.substring(0, this.parameter.indexOf(' '))),
					Paths.get(CommandeFactory.CURRENT_FILE + "\\"+ this.parameter.substring(this.parameter.indexOf(' '), parameter.length())));
			System.out.println("Copie effectu�e !");
		} catch(IOException e) {
			System.out.println("fichier inexistant!");

		}
	}

	public static String getDESC() {
		return DESC;
	}


	public static void setDESC(String dESC) {
		DESC = dESC;
	}


	 // reconnait la commande copy
	
	public static void chargerStaticPortion() {
		// TODO Auto-generated method stub
		
	}
	

}
