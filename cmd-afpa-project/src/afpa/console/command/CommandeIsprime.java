package afpa.console.command;


import afpa.console.exception.CommandException;

public class CommandeIsprime extends AbstractCommandeAvecParam {

	public static final String CMD = "isprime";
	private static final String DESC = "indique si un nombre est premier ou pas";
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}
	
	public static void chargerStaticPortion() {
	}
	
	

	public CommandeIsprime(String c) {
		super(c, CMD);
	}



	@Override
	public String getName() {
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		int nbre = Integer.parseInt(""+this.parameter);
		String res;
		if ((nbre == 2) || (nbre == 3) || (nbre == 4) ||(nbre == 5)) {
			res = "yes";
		} else if((nbre % 2 == 0) || (nbre % 3 == 0) || (nbre % 4 == 0) || (nbre % 5 == 0)) {
			res = "no";
		} else {
			res = "yes";
		}
		System.out.println(res);
	}
	
	
}
